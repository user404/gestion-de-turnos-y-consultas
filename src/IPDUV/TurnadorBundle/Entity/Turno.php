<?php

namespace IPDUV\TurnadorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Turno
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Turno
{

    /**
     * @var string
     *
     */
    private $ajax_id;


    /**
     * Set ajax_id
     *
     * @param integer $ajax_id
     */
    public function setAjaxId($ajax_id)
    {
        $this->ajax_id = $ajax_id;

        return $this;
    }

    /**
     * Get ajax_id
     *
     * @return integer 
     */
    public function getAjaxId()
    {
        return $this->ajax_id;
    }
    

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    *ORM\OneToMany(targetEntity="Consulta", mappedBy="turno")
    **/
    private $consultas;

    /**
     * @ORM\ManyToOne(targetEntity="\IPDUV\UserBundle\Entity\Area", inversedBy="turno")
     * @ORM\JoinColumn(name="area_id", referencedColumnName="id")
     */
    protected $area;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero", type="integer")
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="letra", type="string", length=5)
     */
    private $letra;

    /**
     * @var string
     *
     * @ORM\Column(name="box", type="string", length=5)
     */
    private $box;



    /**
     * Set numero
     *
     * @param integer $numero
     * @return Turno
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set letra
     *
     * @param string $letra
     * @return Turno
     */
    public function setLetra($letra)
    {
        $this->letra = $letra;

        return $this;
    }

    /**
     * Get letra
     *
     * @return string 
     */
    public function getLetra()
    {
        return $this->letra;
    }

    /**
     * Set area
     *
     * @param \IPDUV\UserBundle\Entity\Area $area
     * @return Turno
     */
    public function setArea(\IPDUV\UserBundle\Entity\Area $area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return \IPDUV\UserBundle\Entity\Area 
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set box
     *
     * @param string $box
     * @return Turno
     */
    public function setBox($box)
    {
        $this->box = $box;

        return $this;
    }

    /**
     * Get box
     *
     * @return string 
     */
    public function getBox()
    {
        return $this->box;
    }
}
