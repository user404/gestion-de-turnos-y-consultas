<?php

namespace IPDUV\TurnadorBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IPDUV\TurnadorBundle\Entity\Turno;
use IPDUV\TurnadorBundle\Form\TurnoType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Turno controller.
 *
 * @Route("/turno")
 */
class TurnoController extends Controller
{
    /**
     * @Route("/showajax/", name="show_ajax", options={"expose"=true})
     * @Method("POST")
     */
    public function showAjaxAction() {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Turno')->find(1);
        $array = array(
            'box' => $entity->getLetra(),
            'turno' => $entity->getNumero(),
            'anterior' => $entity->getNumero(),
            'boxAnterior' => $entity->getLetra(),
        );
        
        $response = new JsonResponse();
        $response->setData($array);        
        return $response;
    }
    
    

    /**
     * @Route("/postajax/", name="post_ajax", options={"expose"=true})
     * @Method("POST")
     */
    public function postAjaxAction() {
        $request = $this->getRequest();
        $entity = new Turno();
        $resultado = false;
        $form = $this->createCreateForm($entity);

        $form->handleRequest($request);

        //var_dump($entity);
        //die;

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            
            $resultado=true;
        }
        
        $array = array(
            'resultado' => $resultado,
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;   
    }
    
    /**
     * @Route("/editajax/", name="edit_ajax", options={"expose"=true})
     * @Method("POST")
     */
    public function editAjaxAction(Request $request) {

        $entity = new Turno();
        $resultado = false;
       
        $em = $this->getDoctrine()->getManager();
        $turno = $em->getRepository('IPDUVTurnadorBundle:Turno')->find(1);

        // AUMENTO EL TURNO EN 1
       // $turno->setAnterior($turno->getTurno());
       // $turno->setBoxAnterior($turno->getBox());
        $numero_turno = $turno->getNumero() + 1;
        $turno->setNumero($numero_turno);
        // ASIGNO EL BOX QUE RECUPERE DEL FORMULARIO
        $user = $this->getUser();
        $turno->setBox($user->getBox());
       // $turno->setBox(1);
        
        
      //  var_dump($turno); die;
//        if ($form->isValid()) {
            if($numero_turno >= 100)
            {
                $turno->setNumero(0);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($turno);
            $em->flush();
            
  //      }
        $array = array(
            'resultado' => $numero_turno,
            'box' => $turno->getLetra(),
        );
        
        $response = new JsonResponse();
        $response->setData($array);
        
        return $response;
        
        }

    /**
     * Lists all Turno entities.
     *
     * @Route("/", name="turno")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IPDUVTurnadorBundle:Turno')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Turno entity.
     *
     * @Route("/", name="turno_create")
     * @Method("POST")
     * @Template("IPDUVTurnadorBundle:Turno:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Turno();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('turno_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Turno entity.
     *
     * @param Turno $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Turno $entity)
    {
        $form = $this->createForm(new TurnoType(), $entity, array(
            'action' => $this->generateUrl('turno_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Turno entity.
     *
     * @Route("/new", name="turno_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Turno();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Turno entity.
     *
     * @Route("/{id}", name="turno_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Turno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Turno entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Turno entity.
     *
     * @Route("/{id}/edit", name="turno_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Turno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Turno entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Turno entity.
    *
    * @param Turno $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Turno $entity)
    {
        $form = $this->createForm(new TurnoType(), $entity, array(
            'action' => $this->generateUrl('turno_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));  
        $form->add('ajax_id', 'hidden', array('data' => $entity->getId()));

        return $form;
    }
    /**
     * Edits an existing Turno entity.
     *
     * @Route("/{id}", name="turno_update")
     * @Method("PUT")
     * @Template("IPDUVTurnadorBundle:Turno:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IPDUVTurnadorBundle:Turno')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Turno entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('turno_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Turno entity.
     *
     * @Route("/{id}", name="turno_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IPDUVTurnadorBundle:Turno')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Turno entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('turno'));
    }

    /**
     * Creates a form to delete a Turno entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('turno_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
