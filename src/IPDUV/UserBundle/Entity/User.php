<?php

namespace IPDUV\UserBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
    *ORM\OneToMany(targetEntity="\IPDUV\TurnadorBundle\Entity\Consulta", mappedBy="creador")
    **/
    private $generada;

    /**
    *ORM\OneToMany(targetEntity="\IPDUV\TurnadorBundle\Entity\Consulta", mappedBy="responsable")
    **/
    private $asignada;

     /**
     *@ORM\ManyToOne(targetEntity="Area", inversedBy="usuarios")
     *@ORM\JoinColumn(name="area_id", referencedColumnName="id")
     */
    private $area;

    // public function __construct()
    // {
    //     parent::__construct();
    //     // your own logic
    // }

    
    /**
     * @var integer
     *
     * @ORM\Column(name="box", type="integer")
     */
    private $box;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set box
     *
     * @param integer $box
     * @return User
     */
    public function setBox($box)
    {
        $this->box = $box;

        return $this;
    }

    /**
     * Get box
     *
     * @return integer 
     */
    public function getBox()
    {
        return $this->box;
    }

    /**
     * Set area
     *
     * @param \IPDUV\UserBundle\Entity\Area $area
     * @return User
     */
    public function setArea(\IPDUV\UserBundle\Entity\Area $area = null)
    {
        $this->area = $area;

        return $this;
    }

    /**
     * Get area
     *
     * @return \IPDUV\UserBundle\Entity\Area 
     */
    public function getArea()
    {
        return $this->area;
    }
}
